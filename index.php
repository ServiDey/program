<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Главная</title>
	    <link rel='stylesheet' href="style.css">
		<script src='script.js'></script>
	</head>
	<body>
    <div id='wrapper'>
			<header>
			<nav>
				<ul class='menu'>
          <li class='active'><a href=''>Акты отбора</a>
            <ul class='acts'>
              <li><a href='act/act.php'>Вода водопроводная</a></li>
            </ul>
          </li>
          <li><a href='data.html'>Ввод данных</a></li>
          <li class='active'><a href=''>Справочники</a>
            <ul class='guide'>
                <li><a href='staff/staff.php'>Сотрудники</a></li>
                <li><a href='sampleselection/sampleselection.php'>Госты</a></li>
                <li><a href='devices/devices.php'>Оборудование</a></li>
                <li><a href=''>Реактивы</a></li>
                <li><a href='coefficients/coefficients.php'>Коэффициэнты</a></li>
                <li><a href=''>Журнал учета микроклимата</a></li>
            </ul>
          </li>
          <li><a href=''>Протокол</a></li>
          <li><a href=''>Отчеты</a></li>
          <li><a href=''>Акт списания образцов</a></li>
				</ul>
			</nav>
      </header>
    </body>
</html>